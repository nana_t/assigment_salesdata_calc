package jp.alhinc.tomimatsu_nana.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class CalculateSales {
	public static void main(String[] args) {
		//最後まで使用するデータなのでマップ宣言は上部に
		HashMap<String, String> branch = new HashMap<String, String>();//支店コードと支店名
		HashMap<String, Long> salesData = new HashMap<String, Long>();//支店コードと元値

		//エラー処理（コマンドライン引数）
		if(args.length != 1){
			System.out.println("予期せぬエラーが発生しました");
			return;
		}

		boolean fileRead = canFileRead(args[0],"branch.lst", branch, salesData);
		if(fileRead == false) {
			return;
		}

		//一覧を取得(アレイリストに変更）
		File dir = new File(args[0]);
		File[] dirName = dir.listFiles();
		ArrayList<File> rcdName = new ArrayList<File>();

		//8桁で拡張子.rcdのファイル名を検索（ディレクトリは除く）、抽出
		for(int i = 0; i < dirName.length; i++) {
			if(dirName[i].getName().matches("^\\d{8}.rcd$") && dirName[i].isFile()) {
				rcdName.add(dirName[i]);
			}
		}//←指定ファイルをディレクトリから検索する為のループなので此処で終了

		//エラー処理（連番チェック）
		for(int j = 0; j < rcdName.size()-1; j++) {

			//名前（xxxxxxxx.rcdの.rcdを取り除く）
			String result = rcdName.get(j).getName().substring(0,8);
			String result2 = rcdName.get(j+1).getName().substring(0,8);

			//String型→int型へ変換
			int str = Integer.parseInt(result);
			int str2 = Integer.parseInt(result2);

			//もしも前ファイルとの差分が1ならば、
			if(str2 - str > 1) {
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}
		}

		//特定ファイルの中から必要データを読み込む
		for(int h = 0; h < rcdName.size(); h++) {
			BufferedReader br2 = null;
			try {
				FileReader fr = new FileReader(rcdName.get(h));
				br2 = new BufferedReader(fr);

				String salesList;
				ArrayList<String> shopSales = new ArrayList<String>();
				while((salesList = br2.readLine()) != null) {

					//ArrayListで要素一つ目二つ目を取り出す
					shopSales.add(salesList);
				} //←リストの要素を取り出す為のループなので此処で終了！

				//エラー処理（売上ファイルの中身が2行以外の時）
				if(shopSales.size() != 2) {
					System.out.println(rcdName.get(h).getName() + "のフォーマットが不正です");
					return;
				}

				//エラー処理（売上金額に数字以外の場合）
				if(!(shopSales.get(1).matches("\\d+"))) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}

				//ArrayListに入った金額の型（String型）をint型に変換
				long changeSales = Long.parseLong(shopSales.get(1));

				//MapがArrayListの支店コードを持っているとしたら
				if(salesData.containsKey(shopSales.get(0))) {

					//足し算(足した後にMap(shopSales.get(0),)に戻してる）
					salesData.put(shopSales.get(0), changeSales + salesData.get(shopSales.get(0)));

					//エラー処理（合計金額が10桁を超える場合）
					if(String.valueOf(salesData.get(shopSales.get(0))).length() > 10) {
						System.out.println("合計金額が10桁を超えました");
						return;
					}
				} else {
					//支店コード持ってない場合
					System.out.println(rcdName.get(h).getName() + "の支店コードが不正です");
					return;
				}

				//エラー処理（支店に該当なし）
				if(!shopSales.get(0).matches("^\\d{3}$") ) {
					System.out.println(rcdName.get(h).getName() + "の支店コードが不正です");
					return;
				}
			} catch(IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return;
			} finally {
				if(br2 != null) {
					try {
						br2.close();
					} catch(IOException e) {
						System.out.println("予期せぬエラーが発生しました");
						return;
					}
				}
			}
		}
		boolean fileWriter = canFileWrite(args[0],"branch.out", branch, salesData);
		if(fileWriter == false) {
			return;
		}
	}

	//読込のメソッド分け
	public static boolean canFileRead(String path, String fileName, HashMap<String, String> keyName, HashMap<String, Long> mount) {
		//ファイルの読み込み
		BufferedReader br = null;
		try {
			File file = new File(path, fileName);

			//エラー処理（ファイルが存在するか否か）
			if (!file.exists()) {
				System.out.println("支店定義ファイルが存在しません");
				return false;
			}

			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;
			while((line = br.readLine()) != null) {

				//取り込んだデータを分割する
				String[] branchData = line.split(",");

				//エラー処理（データのフォーマットが不正か否かの確認）
				if(!branchData[0].matches("^\\d{3}$") || branchData.length != 2) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return false;
				}
				//Mapで分割したデータ（支店番号と支店名）を保持
				keyName.put(branchData[0],branchData[1]);
				//Mapにデータ（支店番号と売り上げ金額）を保持
				mount.put(branchData[0], 0L);
			}

		} catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			if(br != null) {
				try {
					br.close();
				} catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}

	//出力のメソッド分け
	static boolean canFileWrite(String pass, String fileName, HashMap<String, String> keyName, HashMap<String, Long> mount) {
		//出した答えを出力
		BufferedWriter bw = null;
		try {
			File salesTotal = new File(pass, fileName);
			FileWriter fw = new FileWriter(salesTotal);
			bw = new BufferedWriter(fw);

			//支店名分だけ結果を出力したい
			for(String key : keyName.keySet()) {
			bw.write(key + "," + keyName.get(key) + "," + mount.get(key) + '\n');
			}
		} catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			if(bw != null) {
				try {
					bw.close();
				} catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}
}